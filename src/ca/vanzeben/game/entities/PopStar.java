package ca.vanzeben.game.entities;

import ca.vanzeben.game.Game;
import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;
import ca.vanzeben.game.level.Level;

public class PopStar extends MovingEntity {

	public PopStar(int x, int y, int speed, int health) {
		super(x, y, speed, health);
	}

	@Override
	public void render(Screen screen) {
		screen.render(x, y, SpriteSheet.DungeonCrawl, 2, 4, Screen.MirrorDirection.NONE);
	}

	@Override
	public void tick() {
		this.tickCount++;
		moveRandom();

	}

	@Override
	public void handleCollision(Entity e2) {
		if (e2 instanceof Coin) {
			health += ((Coin) (e2)).getValue();
		} else if (e2 instanceof Fireball) {
			Game.getLevel().removeEntity(this);
		} else if (e2 instanceof Player) {
			health -= 5;
		}
		
		if (health < 0) {
			Game.getLevel().removeEntity(this);
		}
	}

}

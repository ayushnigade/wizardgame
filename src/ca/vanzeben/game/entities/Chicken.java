package ca.vanzeben.game.entities;

import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;
/**
 * chickens are immortal and indestructible...
 * @author Ayush Nigade
 *
 */
public class Chicken extends MovingEntity {
	private boolean stopMoving = false;

	public Chicken(int x, int y, int speed, int health) {
		super(x, y, speed, health);
	}

	@Override
	public void render(Screen screen) {
		screen.render(x, y, SpriteSheet.tileSheet, 7, 16, Screen.MirrorDirection.NONE);
	}

	@Override
	public void handleCollision(Entity e2) {
		stopMoving = true;
	}

	@Override
	public void tick() {
		this.tickCount++;
		if(!stopMoving) {
		this.moveRandom();
		}
		// run move method method
	}

}

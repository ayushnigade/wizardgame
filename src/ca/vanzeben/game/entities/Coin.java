package ca.vanzeben.game.entities;

import ca.vanzeben.game.Game;
import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;
import ca.vanzeben.game.level.Level;

public class Coin extends Entity{
	private int value;
	
	public Coin(int x, int y, int value, SpriteSheet sheet) {
		super(x, y, sheet);
		this.value = value;
		System.out.println("Created coin " + value);
	}
	
	//default constructor
	public Coin(int x, int y, int value) {
		super(x, y, SpriteSheet.tileSheet);
		this.value = value;
		System.out.println("Created coin " + value);
	}
	
	@Override
	public void render(Screen screen) {
		screen.render(x, y, spritesheet, 16, 12, Screen.MirrorDirection.NONE);
		System.out.println("Rendered coin " + value);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	@Override
	public void handleCollision(Entity e2) {
		if(e2 instanceof Player)
		Game.getLevel().removeEntity(this);
		
	}

	@Override
	public void tick() {
		tickCount++;
	}

	
}

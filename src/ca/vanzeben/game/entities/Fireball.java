package ca.vanzeben.game.entities;

import ca.vanzeben.game.Game;
import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;

public class Fireball extends MovingEntity {

	public Fireball(int x, int y, int speed, int health) {
		super(x, y, speed, health);
	}

	@Override
	public void render(Screen screen) {
		screen.render(x, y, SpriteSheet.DungeonCrawl, 10, 3, Screen.MirrorDirection.NONE);
	}
	
	@Override
	public void tick() {
		moveRandom();
		tickCount++;
	}
	
	@Override
	public void makeInsideFrameCoordinate() {
		if (0 > leftX() ) { 
			Game.getLevel().removeEntity(this);
		} else if ( rightX() >= Game.getLevel().getLevelWidth()) {
			Game.getLevel().removeEntity(this);
		} 
		
		if ( 0 > topY()) 
			Game.getLevel().removeEntity(this);
		else if (bottomY() >= Game.getLevel().getLevelHeight())
			Game.getLevel().removeEntity(this);
	}
	
	@Override
	public void handleCollision(Entity e2) {
		Game.getLevel().removeEntity(this);
	}
}

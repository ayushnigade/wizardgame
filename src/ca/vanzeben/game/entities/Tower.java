package ca.vanzeben.game.entities;

import ca.vanzeben.game.Game;
import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;
import ca.vanzeben.game.level.Level;

public class Tower extends Entity {
	public final int DEFAULT_HEALTH = 10;
	public final int FIREBALL_SPEED = 10;
	public int health;

	public Tower(int x, int y, int health) {
		super(x, y, SpriteSheet.DungeonCrawl);
		this.health = health;
		// takes default height, width, and health values
	}

	public Tower(int x, int y, int health, SpriteSheet sheet) {
		super(x, y, sheet);
		this.health = health;
	}

	protected void setHealth(int health) {
		this.health = health;
	}

	protected int getHealth() {
		return health;
	}

	public void shootFireball() {
		Game.getLevel().getEntites().add(new Fireball(x + 50, y, 10, 1));

	}

	@Override
	public void tick() {
		if (tickCount % 50 == 0) {
			shootFireball();
		}
		tickCount++;
	}

	public void handleCollision(Entity e2) {
		if(e2 instanceof Player) {
			health--;
		}
		
		if (health < 0) {
			Game.getLevel().removeEntity(this);
		}
	}

	public void render(Screen screen) {
		screen.render(x, y, SpriteSheet.DungeonCrawl, 14, 8, Screen.MirrorDirection.NONE);
	}

}

package ca.vanzeben.game.entities;

import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;

public abstract class Entity {
	protected int x, y;
	protected int height, width;
	protected SpriteSheet spritesheet;
	
	protected int tickCount = 0;

	public Entity(int x, int y, SpriteSheet sheet ) {
		this.x = x;
		this.y = y;
		this.width = sheet.getSpriteWidth();
		this.height = sheet.getSpriteHeight();
		this.spritesheet = sheet;
	}

	public int getX() {
		return x;
	}

	public int leftX() {
		return x;
	}

	public int rightX() {
		return leftX() + this.width;
	}

	public int getY() {
		return y;
	}

	public int topY() {
		return y;
	}

	public int bottomY() {
		return topY() + this.height;
	}

	public abstract void render(Screen screen);

	public abstract void tick();

	public void handleCollision(Entity e2) {
		// By default, no collisions are handled.
		// Override this method in child classes
	}
}

package ca.vanzeben.game.entities;

import ca.vanzeben.game.Game;
import ca.vanzeben.game.gfx.Screen;
import ca.vanzeben.game.gfx.SpriteSheet;
import ca.vanzeben.game.level.tiles.Loc;
import ca.vanzeben.game.level.tiles.Tile;

public abstract class MovingEntity extends Entity {
	protected int speed;
	protected int health;
	protected double xSpeed, ySpeed;
	protected boolean isStoppedBySolidTiles = false;

	public MovingEntity(int x, int y, int speed, int health) {
		super(x, y, SpriteSheet.DungeonCrawl);
		this.speed = speed;
		this.health = health;
	}

	protected void setSpeed(int speed) {
		this.speed = speed;
	}

	protected int getSpeed() {
		return speed;
	}

	protected void setHealth(int health) {
		this.health = health;
	}

	protected int getHealth() {
		return health;
	}

	public void setRandomDirection() {
		double randomAngle = Math.random() * 2 * Math.PI;
		double xComponent = Math.cos(randomAngle);
		double yComponent = Math.sin(randomAngle);

		this.xSpeed = speed * xComponent;
		this.ySpeed = speed * yComponent;
	}

	public void moveRandom() {
		if ((tickCount / 60) != (tickCount + 1) / 60) {
			setRandomDirection();
		}
		x += xSpeed;
		y += ySpeed;
		makeInsideFrameCoordinate();
	}

	public void setDirection(Loc loc) {
		double angle = Math.atan2(loc.getY(), loc.getX());
		double xComponent = Math.cos(angle);
		double yComponent = Math.sin(angle);

		this.xSpeed = speed * xComponent;
		this.ySpeed = speed * yComponent;
	}

	public void moveRandomTowards(Loc loc) {
		// need to fix--but D says you can have what you think is good, sooo should I
		// fix?
		//double random = Math.random();
		int oneOrZero = 0;
		if ((tickCount / 60) != (tickCount + 1) / 60) {
			if (oneOrZero == 0) {
				//random = Math.random();
				setRandomDirection();
				oneOrZero++;
			} else {
				//random = Math.random();
				setDirection(loc);
				oneOrZero--;
			}
		}
		x += xSpeed;
		y += ySpeed;
		
		makeInsideFrameCoordinate();
	}
	
	

	public void moveToWorldCoordinates(int x_target, int y_target) {
		setDirection(new Loc(x_target, y_target));
		x += xSpeed;
		y += ySpeed;
		makeInsideFrameCoordinate();
	}

	protected void moveTowardEntity(Entity target) {
		moveToWorldCoordinates(target.getX(), target.getY());
	}
	
	protected void moveTowardsScreenCoordinates(int sx, int sy) {
		sx = Game.getScreen().screenXCoordToWorldCoord(sx);
		sy = Game.getScreen().screenYCoordToWorldCoord(sy);
		
		moveToWorldCoordinates(sx, sy);
	}

	public void moveTowards(Loc loc) {
		setDirection(loc);
		x += xSpeed;
		y += ySpeed;
		makeInsideFrameCoordinate();
	}
	
	public void makeInsideFrameCoordinate() {
		if (0 > leftX() ) { 
			x =  0;
		} else if ( rightX() >= Game.getLevel().getLevelWidth()) {
			x = Game.getLevel().getLevelWidth() - this.width;
		} 
		
		if ( 0 > topY()) 
			y = 0;
		else if (bottomY() >= Game.getLevel().getLevelHeight())
			x = Game.getLevel().getLevelHeight() - this.height;
	}

	public abstract void render(Screen screen);

	public abstract void tick();
}
